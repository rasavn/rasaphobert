# 1. Giới thiệu
Project RasaPhoBERT là chương trình demo sử dụng PhoBERT để phân loại intent trong Rasa

# 2. Cài đặt
* Rasa
```commandline
pip install rasa
```

* Thư viện VNCoreNLP

Cài đặt Java 
```commandline
sudo apt-get install openjdk-8-jdk
```

Thư viện VNCore NLP
```commandline
pip install py_vncorenlp
```

* Thư viện Pytorch
```commandline
pip install torch
```

* Thư viện HuggingFace
```commandline
pip install transformer
```
# 3. Cách sử dụng
* Bước 1. Update dữ liệu training
* Bước 2. Train model
```commandline
rasa train
```
* Bước 3. Thử nghiệm
```commandline
rasa shell nlu
```
# 4. Đánh giá
Bổ sung sau

# 5. Hỗ trợ

Liên hệ: [tungdt.bk@gmail.com](mail:tungdt.bk@gmail.com)