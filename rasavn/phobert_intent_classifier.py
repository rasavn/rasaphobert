import logging
from typing import Any, Text, Dict, List, Type, Tuple

import joblib
from scipy.sparse import hstack, vstack, csr_matrix
from sklearn.linear_model import LogisticRegression

from rasa.engine.storage.resource import Resource
from rasa.engine.storage.storage import ModelStorage
from rasa.engine.recipes.default_recipe import DefaultV1Recipe
from rasa.engine.graph import ExecutionContext, GraphComponent
from rasa.nlu.classifiers import LABEL_RANKING_LENGTH
from rasa.nlu.featurizers.featurizer import Featurizer
from rasa.nlu.classifiers.classifier import IntentClassifier
from rasa.shared.nlu.training_data.training_data import TrainingData
from rasa.shared.nlu.training_data.message import Message
from rasa.shared.nlu.constants import (
    INTENT,
    TEXT,
    INTENT_NAME_KEY,
    PREDICTED_CONFIDENCE_KEY,
)
from rasa.utils.tensorflow.constants import RANKING_LENGTH

import torch
from torch.utils.data import Dataset, DataLoader
from transformers import RobertaForSequenceClassification, AutoTokenizer, AdamW
import json
import os
import py_vncorenlp

logger = logging.getLogger(__name__)


@DefaultV1Recipe.register(
    DefaultV1Recipe.ComponentType.INTENT_CLASSIFIER, is_trainable=True
)
class PhoBERTIntentClassifier(IntentClassifier, GraphComponent):
    """Intent classifier using the PhoBERT."""

    @classmethod
    def required_components(cls) -> List[Type]:
        """Components that should be included in the pipeline before this component."""
        return []

    @staticmethod
    def required_packages() -> List[Text]:
        """Any extra python dependencies required for this component to run."""
        return []

    @staticmethod
    def get_default_config() -> Dict[Text, Any]:
        """The component's default config (see parent class for full docstring)."""
        return {
            "batch_size": 100,
            "epochs": 100,
            "learning_rate": 1e-5
        }

    def __init__(
            self,
            config: Dict[Text, Any],
            name: Text,
            model_storage: ModelStorage,
            resource: Resource,
    ) -> None:
        """Construct a new classifier."""
        self.name = name
        self.config = {**self.get_default_config(), **config}

        self.classifier = PhoBERTClassifier()
        current_path = str(os.getcwd())
        vncorenlp_path = current_path + "/vncorenlp"
        if not os.path.exists(vncorenlp_path):
            os.makedirs(vncorenlp_path)
            py_vncorenlp.download_model(save_dir=vncorenlp_path)
        self.vncorenlp = py_vncorenlp.VnCoreNLP(annotators=["wseg"], save_dir=vncorenlp_path)
        os.chdir(current_path)
        self.intent_id_dict = None

        # We need to use these later when saving the trained component.
        self._model_storage = model_storage
        self._resource = resource

    def train(self, training_data: TrainingData) -> Resource:
        """Train the intent classifier on a data set."""
        intent_type_num = len(training_data.intents)
        data = self._convert_training_data(training_data)
        batch_size = self.config["batch_size"]
        epochs = self.config["epochs"]
        learning_rate = self.config["learning_rate"]
        self.classifier.train(intent_type_num, data, batch_size=batch_size, epochs=epochs, learning_rate=learning_rate)
        self.persist()

        return self._resource

    @classmethod
    def create(
            cls,
            config: Dict[Text, Any],
            model_storage: ModelStorage,
            resource: Resource,
            execution_context: ExecutionContext,
    ) -> "PhoBERTIntentClassifier":
        """Creates a new untrained component (see parent class for full docstring)."""
        return cls(config, execution_context.node_name, model_storage, resource)

    def process(self, messages: List[Message]) -> List[Message]:
        """Return the most likely intent and its probability for a message."""
        for idx, message in enumerate(messages):
            text = self.vncorenlp.word_segment(message.get(TEXT))[0]
            intent_id, confidence = self.classifier.predict(text)
            intent_name = self._get_intent_from_id(intent_id)
            intent = {
                INTENT_NAME_KEY: intent_name,
                PREDICTED_CONFIDENCE_KEY: confidence
            }

            message.set(INTENT, intent, add_to_output=True)
        return messages

    def persist(self) -> None:
        """Persist this model into the passed directory."""
        with self._model_storage.write_to(self._resource) as model_dir:
            self.classifier.save(model_dir)
            logger.debug(f"Saved intent classifier to '{model_dir}'.")

    @classmethod
    def load(
            cls,
            config: Dict[Text, Any],
            model_storage: ModelStorage,
            resource: Resource,
            execution_context: ExecutionContext,
            **kwargs: Any,
    ) -> "PhoBERTIntentClassifier":
        """Loads trained component (see parent class for full docstring)."""
        try:
            with model_storage.read_from(resource) as model_dir:
                component = cls(
                    config, execution_context.node_name, model_storage, resource
                )
                component.classifier = PhoBERTClassifier(model_path=model_dir)
                file_path = str(model_dir) + "/intent_ids.json"
                with open(file_path, 'r') as file:
                    component.intent_id_dict = json.load(file)
                return component
        except ValueError:
            logger.debug(
                f"Failed to load {cls.__class__.__name__} from model storage. Resource "
                f"'{resource.name}' doesn't exist."
            )
            return cls.create(config, model_storage, resource, execution_context)

    @classmethod
    def validate_config(cls, config: Dict[Text, Any]) -> None:
        """Validates that the component is configured properly."""
        pass

    def _convert_training_data(self, training_data: TrainingData) -> List:
        """Process the training data."""
        data = []
        intent_ids = self._save_intent_id(training_data.intents)
        for e in training_data.intent_examples:
            intent_id = intent_ids[e.get(INTENT)]
            text_raw = e.get(TEXT).replace(';', '')
            text = self.vncorenlp.word_segment(text_raw.replace('.', ''))[0]
            data.append((int(intent_id), text))

        return data

    def _save_intent_id(self, intents) -> Dict:
        intent_id_dict = {intent: index for index, intent in enumerate(intents)}
        with self._model_storage.write_to(self._resource) as model_dir:
            file_path = str(model_dir) + "/intent_ids.json"
            with open(file_path, 'w') as file:
                json.dump(intent_id_dict, file)

        return intent_id_dict

    def _get_intent_from_id(self, intent_id):
        for intent, id in self.intent_id_dict.items():
            if id == intent_id:
                return intent

        return None


class IntentDataset(Dataset):
    def __init__(self, data, tokenizer):
        self.data = data
        self.tokenizer = tokenizer

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        label, text = self.data[idx]
        inputs = self.tokenizer.encode_plus(
            text,
            add_special_tokens=True,
            max_length=256,
            padding="max_length",
            truncation=True,
            return_tensors="pt"
        )
        return {
            "input_ids": inputs["input_ids"].squeeze(),
            "attention_mask": inputs["attention_mask"].squeeze(),
            "label": torch.tensor(label)
        }

    def load_data(self, data_path):
        data = []
        with open(data_path, "r", encoding="utf-8") as file:
            for line in file:
                label, text = line.strip().split(";")
                data.append((int(label), text))
        return data


class PhoBERTClassifier:
    def __init__(self, model_path=None):
        self.num_classes = None
        self.model_path = model_path
        self.model = None
        self.tokenizer = None
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        if self.model_path:
            self.load()

    def train(self, num_classes, data, batch_size=8, epochs=5, learning_rate=1e-5):
        self.num_classes = num_classes
        self.model = RobertaForSequenceClassification.from_pretrained("vinai/phobert-base-v2",
                                                                      num_labels=self.num_classes)
        self.tokenizer = AutoTokenizer.from_pretrained("vinai/phobert-base-v2")
        self.model.to(self.device)
        dataset = IntentDataset(data, self.tokenizer)
        dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)

        self.model.train()

        optimizer = AdamW(self.model.parameters(), lr=learning_rate)

        for epoch in range(epochs):
            total_loss = 0
            for batch in dataloader:
                input_ids = batch["input_ids"].to(self.device)
                attention_mask = batch["attention_mask"].to(self.device)
                labels = batch["label"].to(self.device)

                optimizer.zero_grad()

                outputs = self.model(input_ids=input_ids, attention_mask=attention_mask, labels=labels)
                loss = outputs.loss
                total_loss += loss.item()

                loss.backward()
                optimizer.step()

            print(f"Epoch {epoch + 1} - Loss: {total_loss / len(dataloader):.4f}")

    def save(self, save_path):
        if self.model:
            self.model.save_pretrained(save_path)
            self.tokenizer.save_pretrained(save_path)

    def load(self):
        self.model = RobertaForSequenceClassification.from_pretrained(self.model_path)
        self.tokenizer = AutoTokenizer.from_pretrained(self.model_path)
        self.model.to(self.device)

    def predict(self, text):
        self.model.eval()
        with torch.no_grad():
            inputs = self.tokenizer.encode_plus(
                text,
                add_special_tokens=True,
                max_length=256,
                padding="max_length",
                truncation=True,
                return_tensors="pt"
            )
            input_ids = inputs["input_ids"].to(self.device)
            attention_mask = inputs["attention_mask"].to(self.device)

            outputs = self.model(input_ids=input_ids, attention_mask=attention_mask)
            logits = outputs.logits
            predicted_class = torch.argmax(logits, dim=1).item()
            probabilities = torch.softmax(logits, dim=1)
            confidence = probabilities[0][predicted_class].item()

        return predicted_class, confidence
